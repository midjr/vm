/*
 * 
 *  Marc Dean Jr.
 *  CIS 356 - Ladd
 *  VMtranslator - Project 7
 *  Due: 11/18/2013
 *
 */


// The purpose of VMtranslator is to gain an understanding
// of the virtual machine as per the Hack machine from the 
// nand2tetris course. (nand2tetris.org).

// This approach follows the modules in the text book. (Pgs. 143-146)

// To RUN: Vmtranslator <name_of_file.vm> or VMtranslator <name_of_directory.vm>

// To do: implement the main program using parser and code writer
import java.util.*;
import java.io.*;

public class VMtranslator {

	public static void main(String[] args) throws FileNotFoundException {

		// if user forgot a file
		if(args.length == 0){
			System.out.println("\nUsage: $~/VMtranslator <name_of_file.vm>|<name_of_directory>");
			System.exit(0);
		}

		File input;

		for(int i = 0; i < args.length; i++){
			// check if file is a directory
			String s = args[i];
			input = new File(s);
			if(input.isDirectory()){
				System.out.println("Directory");
				String[] files = input.list();
				for(int j = 0; j < files.length; j++){
					if(files[j].toLowerCase().contains(".vm")){
						Parser p = new Parser(files[j].toString());
						CodeWriter c = new CodeWriter(files[j].toString());
						while(p.hasMoreCommands()){
							p.advance();
							if(p.commandType() == Parser.C_ARITHMETIC){
								System.out.println("C ");
								c.writeArithmetic(p.arg1());
							}
							if(p.commandType() == Parser.C_PUSH || p.commandType() == Parser.C_POP){
								c.writePushPop(p.commandType(),p.arg1(), p.arg2() );
							}
						}
						c.close();
					}		
            
        }
    }
    if(s.contains(".vm") && input.exists()) {
    					System.out.println("A File");
						Parser p = new Parser(args[i]);
						CodeWriter c = new CodeWriter(args[i]);
						while(p.hasMoreCommands()){
							p.advance();
							if(p.commandType() == Parser.C_ARITHMETIC){
								c.writeArithmetic(p.arg1());
							}
							if(p.commandType() == Parser.C_PUSH || p.commandType() == Parser.C_POP){
								c.writePushPop(p.commandType(),p.arg1(), p.arg2() );
							}
						}
						c.close();
    }
  }
 }
}
