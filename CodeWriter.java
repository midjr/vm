/*
 *
 *
 *	Marc Dean Jr.
 *  CIS 356 - Ladd
 *  Project 7 - VMtranslator
 *  This portion will also be updated for Project 8
 *  Project 7 Due 11/18/2013
 */

// The CodeWriter class is responsible for translating VM commands
// into Hack assembly code from the nand2tetris course.


import java.util.*;
import java.io.*;

public class CodeWriter {

	// fields:
	
	public PrintStream outWriter;  // print to a file
	public String fName;		// name of file
	public int truths;			// keep track of jumps/labels
	// constructor:

	// Purpose: CodeWriter opens the output file so we can 
	// print our instructions to it.
	// Preconditions: none
	// Postconditions: must set file name with setFileName()
	// Returns: none

	public CodeWriter(String fileName) throws FileNotFoundException {
		this.setFileName(fileName);
	}

	// methods:

	// Purpose: setFileName begins the translation of a new VM file
	// and makes the PrintStream print to the correct file
	// Precondition: CodeWriter established
	// Postcondition: none
	// Returns: none 

	private void setFileName (String fileName) throws FileNotFoundException {
		this.fName = fileName;
		fileName = fileName.substring(0,fileName.indexOf('.'));
		fileName = fileName + ".asm";
		File file = new File(fileName);
		this.outWriter = new PrintStream(file);
		this.truths = 0;
	}


	// Purpose: writes the hack assembly code 
	// for the corresponding vm arithmetic command
	// Precondtion: command is an arithmetic command
	// Postcondition: none
	// Return: none

	public void writeArithmetic(String com) {

		// We want to take the two top values from the 
		// stack and do the operation (if the operation requires
		// two values, else use one) and then push the outcome back on
		// to the top of the stack

		if(com.equalsIgnoreCase("ADD")){
			outWriter.println("// " + com);
			outWriter.print("@SP\nAM=M-1\nD=M\nA=A-1\nM=D+M\n");
			return;
		}
		if(com.equalsIgnoreCase("NEG")){
			outWriter.println("// " + com);
			outWriter.print("@SP\nA=M-1\nM=-M\n");
			return;
		}
		if(com.equalsIgnoreCase("SUB")){
			outWriter.println("// " + com);
			outWriter.print("@SP\nAM=M-1\nD=M\nA=A-1\nM=M-D\n");
			return;
		}
		if(com.equalsIgnoreCase("AND")){
			outWriter.println("// " + com);
			outWriter.print("@SP\nAM=M-1\nD=M\nA=A-1\nM=D&M\n");
			return;
		}
		if(com.equalsIgnoreCase("NOT")){
			outWriter.println("// " + com);
			outWriter.print("@SP\nA=M-1\nM=!M\n");
			return;
		}
		if(com.equalsIgnoreCase("OR")) {
			outWriter.println("// " + com);
			outWriter.print("@SP\nAM=M-1\nD=M\nA=A-1\nM=D|M\n");
			return;
		}

		// use EQ true + truths to make labels,
		// see notes for example from class
		if(com.equalsIgnoreCase("EQ")){
			outWriter.println("// " + com);
			outWriter.print("@SP\nAM=M-1\nD=M\nA=A-1\nD=M-D\n@EQ_T_"+this.truths
							+ "\nD;JEQ\n@SP\nA=M-1\nM=0\n@EQ_N_"+this.truths
							+ "\n0;JMP\n+(EQ_T_"+this.truths+")\n@SP\nA=M-1\n"
							+ "M=-1\n(EQ_N_"+this.truths+")\n");
			this.truths++; // keep track for making labels
		}
		// LT is the same, except switch EQ with LT
		if(com.equalsIgnoreCase("LT")){
			outWriter.println("// " + com);
			outWriter.print("@SP\nAM=M-1\nD=M\nA=A-1\nD=M-D\n@LT_T_"+this.truths
							+ "\nD;JLT\n@SP\nA=M-1\nM=0\n@LT_N_"+this.truths
							+ "\n0;JMP\n+(LT_T_"+this.truths+")\n@SP\nA=M-1\n"
							+ "M=-1\n(LT_N_"+this.truths+")\n");
			this.truths++; // keep track for making labels
		}
		// GT is the same, except switch LT with GT
		if(com.equalsIgnoreCase("GT")){
			outWriter.println("// " + com);
			outWriter.print("@SP\nAM=M-1\nD=M\nA=A-1\nD=M-D\n@GT_T_"+this.truths
							+ "\nD;JGT\n@SP\nA=M-1\nM=0\n@GT_N_"+this.truths
							+ "\n0;JMP\n+(GT_T_"+this.truths+")\n@SP\nA=M-1\n"
							+ "M=-1\n(GT_N_"+this.truths+")\n");
			this.truths++; // keep track for making labels
		}

	}

	// Purpose: writes the hack assembly code for the corresponding
	// command when the command is of type push or pop
	// Precondition: should be a push or pop command
	// Postcondition: none
	// Returns: none

	public void writePushPop (int command, String segment, int index) {
		// to do: write method, read up on translation

		if(command == Parser.C_PUSH){
			outWriter.println("// " + command + " " + segment + " " + index);
			outWriter.print(writePush(segment, index));
			return;
		}
		if(command == Parser.C_POP){
			outWriter.println("// " + command + " " + segment + " " + index);
			outWriter.print(writePop(segment, index));
			return;
		}
		// testing purposes
		System.out.println("Error for push pop on " + command);

	}

	// Purpose: writePop writes the hack commands for the pop vm commands
	// Precondition: must be pop commands
	// Postcondition: none
	// Returns: hack commands

	private String writePop(String seg, int ind){
		// Pop to store values in a certain location
		// make sure you reposition the stack pointer
		// at the end

		// String p finishes each command and resets some of the 
		// mem locations/pointers

		String p = "@R13\nM=D\n@SP\nAM=M-1\nD=M\n@R13\nA=M\nM=D\n";

		if(seg.equalsIgnoreCase("Local")){
			outWriter.println("// " + seg + " " + ind);
			return ("@LCL\nD=M\n@" + ind + "\nD=D+A\n" + p );
		}

		if(seg.equalsIgnoreCase("argument")){
			outWriter.println("// " + seg + " " + ind);
			return "@ARG\nD=M\n@"+ind+"\nD=D+A\n" + p;
		}

		if(seg.equalsIgnoreCase("This")){
			outWriter.println("// " + seg + " " + ind);
			return "@THIS\nD=M\n@"+ind+"\nD=D+A\n" + p;
		}
		if(seg.equalsIgnoreCase("that")){
			outWriter.println("// " + seg + " " + ind);
			return "@THAT\nD=M\n@"+ind+"\nD=D+A\n" + p;
		}
		if(seg.equalsIgnoreCase("Static")){
			outWriter.println("// " + seg + " " + ind);
			return "@"+this.fName + "." +ind+"\nD=A\n" + p;
		}
		if(seg.equalsIgnoreCase("temp")){
			outWriter.println("// " + seg + " " + ind);
			return "@R5\nD=A\n@"+ind+"\nD=D+A\n" + p;
		}

		return "Something is wrong" + seg + "\n\n";
	}


	// Purpose: writePush returns the hack commands
	// for certain push commands in vm. They all have a
	// similar pattern but changes where the values are 
	// pushed based on seg.
	// Precondition: must be a push command only
	// Postcondition: none
	// Returns: hack commands

	private String writePush(String seg, int ind) {
		// taken from class notes, changed a little
		// from how I drew my notes/stack

		// first part of stack after SP is LCL
		if(seg.equalsIgnoreCase("Local")){
			outWriter.println("// " + seg + " " + ind);
			return ("@LCL\nD=M\n@" + ind + "\nA=D+A\nD=M\n" +
					"@SP\nA=M\nM=D\n@SP\nM=M+1\n");
		}

		// next section is args
		if(seg.equalsIgnoreCase("Argument")){
			outWriter.println("// " + seg + " " + ind);
			return ("@ARG\nD=M\n@" + ind + "\nA=D+A\nD=M\n" + 
					"@SP\nA=M\nM=D\n@SP\nM=M+1\n");
		}
		// next is this, that
		if(seg.equalsIgnoreCase("THIS")){
			outWriter.println("// " + seg + " " + ind);
			return ("@THIS\nD=M\n@" + ind + "\nA=D+A\nD=M\n"
				   + "@SP\nA=M\nM=D\n@SP\nM=M+1\n");
		}

		if(seg.equalsIgnoreCase("THAT")){
			outWriter.println("// " + seg + " " + ind);
			return ("@THAT\nD=M\n@" + ind +"\nA=D+A\nD=M\n"
				    + "@SP\nA=M\nM=D\n@SP\nM=M+1\n");
		}

		// constant @7 D=A
		if(seg.equalsIgnoreCase("constant")){
			outWriter.println("// " + seg + " " + ind);
			return("@" + ind +"\nD=A\n@SP\nA=M\nM=D\n@SP\nM=M+1\n");
		}

		if(seg.equalsIgnoreCase("temp")){
			// temp register # 5, local variable, top of 
			// stack
			outWriter.println("// " + seg + " " + ind);
			return ("@R5\nD=A\n@"+ind+"\nA=D+A\nD=M\n" 
					+ "@SP\nA=M\nM=D\n@SP\nM=M+1\n");
		}

		// slide 25 (lecture) static is mapped to filename.i
		// and assembly maps symbols to Ram
		if(seg.equalsIgnoreCase("STATIC")){
			outWriter.println("// " + seg + " " + ind);
			return ("@" + this.fName + "." + ind +"\nD=M\n"
					+ "@SP\nA=M\nM=D\n@SP\nM=M+1\n");
		}

		return "problem with " + seg + " \n\n";

	}

	// Purpose: close closes the file stream
	// Precondition: file is open
	// Postcondition: none
	// Returns: none
	
	public void close() {
		this.outWriter.close();
	}



}