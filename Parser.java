/*
 *
 *
 *	Marc Dean Jr.
 *  CIS 356 - Ladd
 *  VMtranslator - Project 7
 *  Due: 10/18/2013
 */


 // The parser module handles the .vm file and gives access
 // to the input code. It reads VM commands, parses them, then
 // provides access to their components. Ignores white space and 
 // comments.

 import java.util.*;	// scanner
 import java.io.*;		// file i/o

 // List of command types as constants for
 // ease of remembering and using

 public class Parser {

 	// constants
 	final static int C_ARITHMETIC = 1;
 	final static int C_PUSH = 2;
 	final static int C_POP = 3;
 	final static int C_LABEL = 4;
 	final static int C_GOTO = 5;
 	final static int C_IF = 6;
 	final static int C_FUNCTION = 7;
 	final static int C_RETURN = 8;
 	final static int C_CALL = 9;

 	// fields:
 	public Scanner file;		// file to be read with vm commands
 	public String currentCommand; // current command read from file
 	public String fN; 			// name of file -- handy for CodeWriter




 	// constructor:

 		// Purpose: Parser prepares the .vm file to be read/parsed
 		// Precondition: none
 		// Postcondition: none
 		// Returns: none

 		public Parser (String fileName) throws FileNotFoundException {
 			
 			// if the file is a .vm file, continue, else display usage

 			if(!fileName.toLowerCase().contains(".vm")) {
 				System.out.println("USAGE: name_of_file.vm - must be a .vm file");
 				System.exit(0);
 			} 

 			this.fN = fileName;


 			// current command should be essentially nothing
 			this.currentCommand = "";

 			// use the name of the file to make a file
 			// make sure it exists
 			File f = new File(fileName);

 			// if it exists, create a scanner on it, 
 			// otherwise through an exception/exit
 			if(f.exists()) 
 				this.file = new Scanner(f);
 			   else 
 			   	 throw new FileNotFoundException(fileName + " is not found!");

 		}

 		// methods:


 		// Purpose: hasMoreCommands checks if there are any more commands
 		// in the .vm file
 		// Precondition: none
 		// Postcondition: none
 		// Returns: true if there are more lines, (does not take into account
 		// white space or blank lines)

 		public boolean hasMoreCommands() {
 			return this.file.hasNextLine();
 		}

 		// Purpose: advance puts the next command into currentCommand
 		// Preconditon: hasMoreCommands() is true, no spacing before commands
 		// Postconditon: none
 		// Returns: none

 		public void advance() {
 			// check if there are more commands
 			if(this.hasMoreCommands()){
 				// put the next line into current command
 				this.currentCommand = this.file.nextLine();
 			}
 				// if this current command is a blank, or starts with
 				// a '/' indicating a comment, continue through the file
 				while((this.currentCommand.equals("") || 
 					this.currentCommand.charAt(0) == '/') && this.hasMoreCommands()){
 					this.currentCommand = this.file.nextLine();
 			    }

 			// remove leading/trailing spaces for easier use in other methods
 			this.currentCommand = this.currentCommand.trim();
 				
 		}

 		// Purpose: commandType gives the type of command of the 
 		// currentCommand from the VM file
 		// Precondition: none
 		// PostCondition: none
 		// Returns: current vm command type (number) or -1 for error
 		// which should screw up the whole program

 		public int commandType() {

 			// For ease of understanding we will use numbers
 			// to define which command they are
 			if(this.currentCommand.toLowerCase().contains("pop"))
 				return C_POP;
 			if(this.currentCommand.toLowerCase().contains("push"))
 				return C_PUSH;
 			if(this.currentCommand.toLowerCase().contains("return"))
 				return C_RETURN;
 			if(this.currentCommand.toLowerCase().contains("call"))
 				return C_CALL;
 			if(this.currentCommand.toLowerCase().contains("function"))
 				return C_FUNCTION;
 			if(this.currentCommand.toLowerCase().contains("label"))
 				return C_LABEL;
 			if(this.currentCommand.toLowerCase().contains("if-goto"))
 				return C_IF;
 			if(this.currentCommand.toLowerCase().contains("goto"))
 				return C_GOTO;
 			if(this.currentCommand.toLowerCase().contains("add"))
 				return C_ARITHMETIC;
 			if(this.currentCommand.toLowerCase().contains("sub"))
 				return C_ARITHMETIC;
 		    if(this.currentCommand.toLowerCase().contains("and"))
 				return C_ARITHMETIC;
 			if(this.currentCommand.toLowerCase().contains("or"))
 				return C_ARITHMETIC;
 			if(this.currentCommand.toLowerCase().contains("not"))
 				return C_ARITHMETIC;
 			if(this.currentCommand.toLowerCase().contains("neg"))
 				return C_ARITHMETIC;
 			// if we haven't returned yet, something is wrong
 			// return a -1 which should throw off the rest of the 
 			// program but also show an error message
 			System.out.println("Error in commandType for " 
 							   + this.currentCommand);
 			return -1;

 		}

 		// Purpose: Returns the first part of the argument of 
 		// the current command, if its a C_ARITHMETIC, should 
 		// return the actual command (ex. add, sub, etc)
 		// Precondition: should not be called when current command
 		// is of type C_RETURN
 		// Postcondition: none
 		// Returns: string value with argument portion
 		// or command if C_ARITHMETIC

 		public String arg1 () {
 			// check to make sure not a return command
 			if(this.commandType() == C_RETURN) {
 				// Print error message to the screen and 
 				// exit
 				System.out.println("Should not be getting argument from"
 					                + " return statement -- Exiting");
 				System.exit(0);
 			}

 			// If we are here, we should be able to parse the arg/com from
 			// this.currentCommand

 			String argument = this.currentCommand;

 			// find the first space (end of first word) and return the substring of it
 			// meaning the first word for C_ARITHMETIC
 			int firstSpace = argument.indexOf(' ');
 			Scanner firstWord = new Scanner(argument);
 			if(this.commandType() == C_ARITHMETIC)
 				return firstWord.next();

 			// still here, not a arithmetic command
 			argument = argument.substring(firstSpace);
 			// return first argument
 			firstWord = new Scanner(argument);
 			return firstWord.next();

 		}

 		// Purpose: arg2 returns the second argument of the current
 		// command. Typically it is an integer value that will be
 		// put on the stack
 		// Precondition: should only be used if current command is
 		// of types C_PUSH, C_POP, C_FUNCTION, or C_CALL
 		// Postcondition: none
 		// Return: int value that is the second argument of the
 		// current command

 		public int arg2 () {
 			// check to make sure we are using correct
 			// current command while we are here
 			int t = this.commandType(); 	// current commandType value to check
 			if(!(t == C_PUSH || t == C_POP || t == C_FUNCTION ||
 				t == C_CALL )) {
 				System.out.println("Called arg2 with improper commandType" + 
 									" Exiting!   " + this.commandType() + this.currentCommand);
 				System.exit(0);
 			}

 			// if we are still here grab the last value (should be an int)
 			// and return it

 			String argument = this.currentCommand;
 			argument = argument.substring(argument.indexOf(' '));

 			// at this position we have removed the first word in the command
 			// remove the next word, but trim the leading space (in case there
 			// are multiple spaces)
 			
 			argument = argument.trim();
 			argument = argument.substring(argument.indexOf(' '));

 			// trim the spaces again and take the substring of from 0 to first 
 			// space, parse it as an integer

 			argument = argument.trim();
 			Scanner getNum = new Scanner(argument);
 			argument = getNum.next();

 			// make an assumption that what is left is a number in string form
 			// parse it to an int value and return it
 			return Integer.parseInt(argument);


 		}


 }